import { IBoss } from "../interface/IBoss";
import { IKiller } from "../interface/IKiller";
class Boss implements IBoss {
    public ki: String;
    constructor(k1: String) {
        this.ki = k1;
    }
    boss(): String {
        console.log("this is Bosss", this.ki)
        return this.ki;
    }
}
export default Boss;