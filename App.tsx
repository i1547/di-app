/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import Boss from './src/testDip/classDip/Boss';
import Sward from './src/testDip/classDip/Sward';

import {
  SampleBosse,
  sss,
  sampleKiller,
} from './src/testDip/sampleobject/SampleObject';
const t = new Sward();
const fff = () => {
  console.log(sampleKiller.kill);
};
const App = () => {
  return (
    <View style={{justifyContent: 'center', alignItems: 'center'}}>
      <TouchableOpacity
        style={{
          backgroundColor: '#9898',
          width: 120,
          height: 120,
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={fff}>
        <Text>کلیک کن</Text>
      </TouchableOpacity>
    </View>
  );
};

export default App;
